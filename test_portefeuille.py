import pytest
from portefeuille import Portefeuille, MontantInsuffisant


@pytest.fixture
def portefeuille_vide():
    '''Returns a Wallet instance with a zero balance'''
    return Portefeuille()


@pytest.fixture
def portefeuille():
    '''Returns a Wallet instance with a balance of 20'''
    return Portefeuille(20)


def test_valeur_par_defaut_portefeuille(portefeuille_vide):
    assert portefeuille_vide.balance == 0


def test_constructeur_portefeuille(portefeuille):
    assert portefeuille.balance == 20


def test_portefeuille_ajouter_argent(portefeuille):
    portefeuille.ajouter_argent(80)
    assert portefeuille.balance == 100


def test_portefeuille_depenser_argent(portefeuille):
    portefeuille.depenser_argent(10)
    assert portefeuille.balance == 10


def test_portefeuille_depenser_argent_MontantInsuffisant(portefeuille_vide):
    with pytest.raises(MontantInsuffisant):
        portefeuille_vide.depenser_argent(100)


@pytest.mark.parametrize("earned,spent,expected", [
    (30, 10, 20),
    (20, 2, 18),
])
def test_transactions(portefeuille_vide, earned, spent, expected):
    portefeuille_vide.ajouter_argent(earned)
    portefeuille_vide.depenser_argent(spent)
    assert portefeuille_vide.balance == expected


@pytest.mark.parametrize("earned,spent", [
    (30, 40),
    (20, 100),
])
def test_transactions_insufficient_amounts(portefeuille_vide, earned, spent):
    portefeuille_vide.ajouter_argent(earned)

    with pytest.raises(Exception):
        portefeuille_vide.depenser_argent(spent)
