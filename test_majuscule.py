import pytest


def majuscule(x):
    if not isinstance(x, str):
        raise TypeError('Veuillez renseigner un parametre de type String')
    return x.capitalize()


def test_majuscule():
    assert majuscule('conception') == 'Conception'
    
def test_majuscule2():
    assert majuscule('logicielle') == 'Logicielle'


def test_raises_exception_on_non_string_arguments():
    with pytest.raises(TypeError):
        majuscule(9)
